package validator;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

//@author Derreck Mansheim 


public class VotingIncrementerTest {

	// getUrlRank
	private String url = "abcnews.com";
	private String voting = "true";

	private String url_to_upvote = "yahoo.com";
	//change value to reflect one more than current value in DB
	private int url_to_upvote_rank = 76;
	

	@Test
	public void getUrlRankTest() {
		VotingIncrementer voteincrement = new VotingIncrementer(url, voting);
		try {
			int newRank = voteincrement.getUrlRank(url);
			Assert.assertEquals(newRank, 99); 
		} catch (Exception e) {
			fail("Test has failed.");
			e.printStackTrace();
		}
	}

	@Test
	public void updateRankTest() { // rank is updated in database
		VotingIncrementer voteincrement = new VotingIncrementer(url_to_upvote, "true");
		try {
			voteincrement.updateRank(url_to_upvote,url_to_upvote_rank );
			int newRank = voteincrement.getUrlRank(url_to_upvote);
			Assert.assertEquals(newRank, url_to_upvote_rank); 
		} catch (Exception e) {
			fail("Test has failed.");
			e.printStackTrace();
		}
	}
	
}
