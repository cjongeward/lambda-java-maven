package validator;

import static org.junit.Assert.*;

import org.junit.Test;

public class UrlCheckerTest {

	@Test
	public void DomainStripperTest() {
		UrlStringCheckTask uc = new UrlStringCheckTask("hello.com");
		assertEquals(uc.getBaseUrl(), "hello.com");
	}
	@Test
	public void DomainStripperHttpTest() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://hello.com");
		assertEquals(uc.getBaseUrl(), "hello.com");
	}
	@Test
	public void DomainStripperWWWTest() {
		UrlStringCheckTask uc = new UrlStringCheckTask("www.hello.com");
		assertEquals(uc.getBaseUrl(), "hello.com");
	}
	@Test
	public void DomainStripperHttpWwwTest() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://www.hello.com");
		assertEquals(uc.getBaseUrl(), "hello.com");
	}
	@Test
	public void DomainStripperSlashAtEnd() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://www.hello.com/");
		assertEquals(uc.getBaseUrl(), "hello.com");
	}
	@Test
	public void DomainStripperStuffatend() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://www.hello.com/blah/blah?xyz=123");
		assertEquals(uc.getBaseUrl(), "hello.com");
	}
	@Test
	public void DomainextentionTest() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://www.hello.com/blah/blah?xyz=123");
		assertEquals(uc.getExtention(), "com");
	}
	@Test
	public void DomainextentionTrickyTest() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://www.hello.com.co/blah/blah?xyz=123");
		assertEquals(uc.getExtention(), "co");
	}
	@Test
	public void CoDomainCheck() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://www.hello.com.co/blah/blah?xyz=123");
		assertTrue(uc.getValue() < 30.01f);
		assertTrue(uc.getValue() > 29.99);
	}
	@Test
	public void ComDomainCheck() {
		UrlStringCheckTask uc = new UrlStringCheckTask("http://www.hello.com/blah/blah?xyz=123");
		assertTrue(uc.getValue() < 50.01f);
		assertTrue(uc.getValue() > 49.99);
	}

}
