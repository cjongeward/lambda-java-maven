package validator;

public class ResponseClass {
    String responseString;
    float rank;
    public ResponseClass() {  
    	this.responseString = "";
    }
    public ResponseClass(String in_responseString) {
    	this.responseString = in_responseString;
    }
    public String getResponseString() {
    	return this.responseString;
    }
    public void setResponseString(String in_responseString) {
    	this.responseString = in_responseString;
    }
    public float getRank() {
    	return this.rank;
    }
    public void setRank(float in_rank) {
    	this.rank = in_rank;
    }
}
