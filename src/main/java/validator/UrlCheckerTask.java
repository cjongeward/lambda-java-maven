package validator;

public interface UrlCheckerTask {
    float getWeight();
    float getValue();
}
