package validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class VotingIncrementer {

	private String url;
	private boolean vote;	

	//Amazon Credentials
	private final String key1 = "AKIAINLTALKWM6B2TLEA";
	private final String key2 = "3dusQmRj9LJDXWoM/IgQdiZIf11XlSkR1Y8bsmRy";	

	// Dynamo DB Specific Detals
	private final String tableName = "UrlRanks";
	private final String urlColumn = "UrlName";
	private final String rankColumn = "UrlRank";
	
	//Define Connections
	private BasicAWSCredentials basic;
	private AmazonDynamoDBClient ddb;	
	private Regions myRegion = Regions.US_WEST_2;
	private DynamoDB dynamodb;

	public VotingIncrementer(String url, String vote) {
		this.url = url;
		if("true".equalsIgnoreCase(vote)){
			this.vote = true; //upvoting
		}else{
			this.vote = false; //downvoting
		}
		// Create Amazon Connections
		basic = new BasicAWSCredentials(key1, key2);
		ddb = new AmazonDynamoDBClient(basic);
		ddb.setRegion(Region.getRegion(myRegion));
		dynamodb = new DynamoDB(ddb);
	}

	public int dovoting() {
		// get the rank from the database
		try{
			int rank = getUrlRank(url);
			//  vote/downvote
			if (vote){
				rank++; 
			} else{
				rank--;
			}
			//update the rank in the database
			updateRank(url, rank);
			return rank;
		} catch(Exception ex){
			ex.printStackTrace();
			return -1;
		}

	}

	void updateRank(String url, int rank) throws Exception{
		final Table table = dynamodb.getTable(tableName);
    	final Item item = new Item().withPrimaryKey(urlColumn, url).withNumber(rankColumn, rank);
	    PutItemOutcome outcome = table.putItem(item);
	}
	
	int getUrlRank(String url) throws Exception {
		int defaultRank = -1;
		final Map<String, AttributeValue> keyValue = new HashMap<>();
		keyValue.put(urlColumn, new AttributeValue(url));
		final GetItemRequest request = new GetItemRequest()
			                       .withKey(keyValue)
			                       .withTableName(tableName);
		final Map<String,AttributeValue> returned_item = ddb.getItem(request).getItem();
		if (returned_item != null) {
			Set<String> keys = returned_item.keySet();
    		for (String key : keys) {
	    		if(key.equals(rankColumn)) {
	    			System.out.println("Rank from the database is " + returned_item.get(key).getN());	    
	    			defaultRank = Integer.valueOf(returned_item.get(key).getN());
	    			break;
			    }
		    }    	
		}
		return defaultRank;
	}

	public static void main(String[] args) {
		VotingIncrementer voteincrement = new VotingIncrementer("wsp.com", "false");
    	int newRank = voteincrement.dovoting();
	}
	
}




