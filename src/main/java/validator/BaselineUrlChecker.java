package validator;

import java.util.HashSet;
import java.util.Set;

public final class BaselineUrlChecker {
	private final String url;
	private Set<UrlCheckerTask> urlCheckerTaskSet;

    BaselineUrlChecker(String in_url) {
         this.url = in_url;   	
         this.urlCheckerTaskSet = new HashSet<>();
         this.constructCheckerTasks();
    }
    
    private void constructCheckerTasks() {
    	this.urlCheckerTaskSet.add(new UrlStringCheckTask(url));
    	this.urlCheckerTaskSet.add(new RandomUrlCheckerTask());
    }
    
    final int getBaselineUrlRank() {
    	float weightSum = 0.f;
    	float valueSum = 0.f;
    	for(UrlCheckerTask task : this.urlCheckerTaskSet) {
    		final float weight = task.getWeight();
    		weightSum += weight;
    		final float value = task.getValue();
    		valueSum += value > weight ? weight : value;
    	}
    	return (int) (valueSum * 100.f / weightSum);
    }
}
