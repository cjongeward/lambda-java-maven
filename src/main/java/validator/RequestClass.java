package validator;

public class RequestClass {
    String requestUrl;
    public RequestClass() {    }
    public RequestClass(String in_requestUrl) {
    	this.requestUrl = in_requestUrl;
    }
    public String getRequestUrl() {
    	return this.requestUrl;
    }
    public void setRequestUrl(String in_requestUrl) {
    	this.requestUrl = in_requestUrl;
    }
}
